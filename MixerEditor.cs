/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios, jthorborg.com]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

#if UNITY_EDITOR
using UnityEngine;
	using UnityEditor;
	using System.Collections;

	namespace Blunt
	{

		namespace Sound
		{
			using DSPType = System.Single;

			[CustomEditor(typeof(Mixer))]
			public class MixerEditor : Editor
			{
				public static DSPType minimumDBVal = -60;
				public static DSPType maximumDBVal = 0;

				public double pole = 0.9997;
				public double filter = 0.0;
				// Add menu item named "My Window" to the Window menu
				//[MenuItem("Window/My Window")]
				public static void ShowWindow()
				{
					//Show existing window instance. If one doesn't exist, make one.
					EditorWindow.GetWindow(typeof(MixerEditor));
				}

				public override bool RequiresConstantRepaint()
				{
					return true;
				}

				public override void OnInspectorGUI() 
				{
					// draw the script functions:
					DrawDefaultInspector ();

					Mixer mixer = (Mixer) target;
					if(mixer != null)
					{
						// set the mixer to enable measurements.
						mixer.enableMixerMeasurements(true);

						var readings = mixer.getReadings ();

						string mixerName = mixer.getName ();
						if(mixerName != null &&  mixerName.Length > 0)
							TextLabel (mixerName);
						else
							TextLabel ("Unnamed mixer");

						string error;

						switch(mixer.getCurrentError())
						{
						case Mixer.ErrorCodes.Disabled:
							error = "Mixer is disabled";
							break;
						case Mixer.ErrorCodes.None:
							error = "Mixer is working";
							break;
						case Mixer.ErrorCodes.ExceptionDuringProcessing:
							error = "Mixer crashed while processing.";
							break;
						case Mixer.ErrorCodes.ThreadingError:
							error = "Unsupported threading mode.";
							break;
						case Mixer.ErrorCodes.UnsupportedNumberOfChannels:
							error = "Mixer was delivered an unsupported number of channels";
							break;
						case Mixer.ErrorCodes.SoundSystemIsLoading:
							error = "Sound subsystem is not completely loaded.";
							break;
						default:
							error = "Unknown mixer state: " + mixer.getCurrentError();
							break;
						}
						TextLabel (error);
						double accumulatedCPU = 0;

						if(readings.Count > 0 )
						{
							TextLabel ("Processing " + readings[0].sampleFrames 
									   + "/" + Blunt.Synthesis.EnvironmentTuning.sampleRate  + " frames");
						}

						foreach(Mixer.Reading channel in readings)
						{

							var timeForProcessing = channel.processingTime;
							TextLabel("Time: " + timeForProcessing.ToString() + " ms");
							var cpuTime = timeToCPUUsage(timeForProcessing, channel.sampleFrames,
														 Blunt.Synthesis.EnvironmentTuning.sampleRate);
							TextLabel ("Usage: " + cpuTime.ToString() + "%");
							accumulatedCPU += cpuTime;
							// display the maximum readings.
							var value = Mathf.Clamp(channel.getMaxDBReadingForChannels(), minimumDBVal, maximumDBVal);
							// as a progress bar (sigh)
							ProgressBar((float)MathExt.UnityScale.Inv.linear(value, minimumDBVal, maximumDBVal), channel.channelName);
						}

						filter = accumulatedCPU + pole * (filter - accumulatedCPU);

						TextLabel ("Total core usage: " + filter.ToString() + "%");
					}
				}
				double timeToCPUUsage(double processingTime, double sampleFrames, double sampleRate)
				{
					var timePerSample = processingTime / sampleFrames;
					var msForASecond = timePerSample * sampleRate;
					return 0.1 * msForASecond; // divided by 1000, times 100 for percentage.
				}
				void ProgressBar (float value, string label) 
				{
					// Get a rect for the progress bar using the same margins as a textfield:
					Rect rect = GUILayoutUtility.GetRect (18, 18, "TextField");
					EditorGUI.ProgressBar (rect, value, label);
				
					EditorGUILayout.Space ();
				}
				void TextLabel(string label)
				{
					Rect rect = GUILayoutUtility.GetRect (18, 18, "TextField");
					EditorGUI.LabelField (rect, label);
					EditorGUILayout.Space ();
				}


			};

		}
	}
#endif